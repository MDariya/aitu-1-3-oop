package kz.aitu.oop.examples.practice3;
import java.sql.*;
public class Main {
    public static void main(String[] args) {
        String url="jdbc:mysql://localhost:3307/practice2";
        String username="root";
        String pass="280102";
        try {
            Connection conn=DriverManager.getConnection(url, username,pass);
            Statement statement=conn.createStatement();
            ResultSet resultSet=statement.executeQuery("select * from employee");
            Department dep1=new Department();
            Project p1=new Project();
            while (resultSet.next()) {
                Employee e=new Employee(resultSet.getInt("id"),resultSet.getString(2),resultSet.getString("position"),resultSet.getDouble("salary"));
                p1.addEmployee(e);
                dep1.addEmployee(e);
            }
            System.out.println(p1.getProjectCost());
            System.out.println(dep1.getTotalSalary());
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }
    }
}

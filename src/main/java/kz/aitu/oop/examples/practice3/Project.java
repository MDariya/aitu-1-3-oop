package kz.aitu.oop.examples.practice3;

import java.util.LinkedList;

public class Project {
    private int id;
    private String name;
    private LinkedList<Employee> employees=new LinkedList<>();

    public Project(int id, String name) {
        this.id = id;
        this.name = name;
    }

    public Project() {
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public LinkedList<Employee> getEmployees() {
        return employees;
    }

    public void setEmployees(LinkedList<Employee> employees) {
        this.employees = employees;
    }
    public double getProjectCost() {
        double cost=0.0;
        for (int i=0;i<employees.size();i++) {
            cost=cost+employees.get(i).getSalary();
        }
        return cost;
    }
    public void addEmployee(Employee a) {
        employees.push(a);
    }
}

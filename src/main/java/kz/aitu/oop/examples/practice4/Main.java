package kz.aitu.oop.examples.practice4;
import java.sql.*;
public class Main {
    public static void main(String[] args) throws SQLException {
        String url="jdbc:mysql://localhost:3307/practice2";
        String username="root";
        String pass="280102";
        try{
            Connection connection = DriverManager.getConnection(url, username, pass);
            Statement statement=connection.createStatement();
            ResultSet fishResult=statement.executeQuery("select *from fish");
            Aquarium a=new Aquarium();
            while (fishResult.next()) {
                Fish fa=new Fish(fishResult.getInt("id"),fishResult.getString("type"),fishResult.getDouble("cost"));
                a.addFish(fa);
            }
            ResultSet repResult=statement.executeQuery("select *from reptelies");
            while (repResult.next()) {
                Reptelies ra=new Reptelies(repResult.getInt("id"),repResult.getString(2),repResult.getDouble(3));
                a.addReptelies(ra);
            }
            ResultSet acResult=statement.executeQuery("select *from accessories");
            while (acResult.next()) {
                Accessories aa=new Accessories(acResult.getInt(1),acResult.getString(2),acResult.getDouble(3));
                a.addAccessories(aa);
            }
            System.out.println(a.totalCost());
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }


    }
}

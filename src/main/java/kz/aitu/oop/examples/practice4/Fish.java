package kz.aitu.oop.examples.practice4;

public class Fish {
    private int id;
    private String type;
    private double cost;

    public Fish(int id, String type, double cost) {
        this.id = id;
        this.type = type;
        this.cost = cost;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public double getCost() {
        return cost;
    }

    public void setCost(double cost) {
        this.cost = cost;
    }
}

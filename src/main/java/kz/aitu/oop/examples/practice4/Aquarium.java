package kz.aitu.oop.examples.practice4;

import java.util.LinkedList;

public class Aquarium {
LinkedList<Fish> fish=new LinkedList<>();
LinkedList<Reptelies> reptelies=new LinkedList<>();
LinkedList<Accessories> accessories=new LinkedList<>();

    public LinkedList<Fish> getFish() {
        return fish;
    }

    public LinkedList<Reptelies> getReptelies() {
        return reptelies;
    }

    public LinkedList<Accessories> getAccessories() {
        return accessories;
    }
    public double totalCost() {
        double cost=0.0;
        for (int i=0;i<fish.size();i++) {
            cost=cost+fish.get(i).getCost();
        }
        for (int i=0;i<reptelies.size();i++) {
            cost=cost+reptelies.get(i).getCost();
        }
        for (int i=0;i<accessories.size();i++) {
            cost=cost+accessories.get(i).getCost();
        }
        return cost;
    }
    public void addFish(Fish a) {
        fish.push(a);
    }
    public void addAccessories(Accessories a) {
        accessories.push(a);
    }
    public void addReptelies(Reptelies a) {
        reptelies.push(a);
    }
}

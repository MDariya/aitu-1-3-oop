package kz.aitu.oop.examples.practice7QUIZ;

import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        FoodFactory foodFactory=new FoodFactory();
        Scanner scanner=new Scanner(System.in);
        String type=scanner.nextLine();
        Food food=foodFactory.getFood(type);
        food.getType();
    }
}

package kz.aitu.oop.examples.practice7QUIZ;

public class FoodFactory {
    public Food getFood(String type) {
        if ("cake".equals(type)) {
            return new Cake();
        }
        else if ("pizza".equals(type)){
            return new Pizza();
        } else {
            return null;
        }
    }

}

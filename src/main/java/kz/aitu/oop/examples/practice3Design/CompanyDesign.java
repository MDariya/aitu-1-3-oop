package kz.aitu.oop.examples.practice3Design;

import kz.aitu.oop.examples.practice3.Company;
import kz.aitu.oop.examples.practice3.Department;

import java.util.LinkedList;

public class CompanyDesign {
    private String name;
    private LinkedList<Department> departments = new LinkedList<>();

    public CompanyDesign () {

    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public LinkedList<Department> getDepartments() {
        return departments;
    }
    public void addDepartment(Department a) {
        departments.push(a);
    }
    public double getTotalSalary() {
        double salary=0.0;
        for (int i=0;i<departments.size();i++) {
            salary=salary+departments.get(i).getTotalSalary();
        }
        return salary;
    }
}

package kz.aitu.oop.examples.practice3Design;

import kz.aitu.oop.examples.practice3.Employee;
import kz.aitu.oop.examples.practice3.Project;

import java.util.LinkedList;

public class DepartmentDesign {
    private LinkedList<Employee> employeesList=new LinkedList<>();
    private LinkedList<Project> projectsList=new LinkedList<>();

    public LinkedList<Employee> getEmployeesList() {
        return employeesList;
    }

    public void setEmployeesList(LinkedList<Employee> employeesList) {
        this.employeesList = employeesList;
    }

    public LinkedList<Project> getProjectsList() {
        return projectsList;
    }

    public void setProjectsList(LinkedList<Project> projectsList) {
        this.projectsList = projectsList;
    }
    public void addEmployee(Employee a) {
        employeesList.push(a);
    }
    public void addProject(Project a) {
        projectsList.push(a);
    }
    public double getTotalSalary() {
        double money=0.0;
        for (int i=0;i<employeesList.size();i++) {
            money=money+employeesList.get(i).getSalary();
        }
        return money;
    }
    public int projectsNumber() {
        return projectsList.size();
    }
    public int employeesNumber() {
        return employeesList.size();
    }

}

package kz.aitu.oop.examples.practice3Design;

public class EmployeeDesign {
    private int id;
    private String name;
    private String position;
    private double salary;

    public EmployeeDesign (EmployeeDesignBuilder a) {
        this.id=a.id;
        this.name=a.name;
        this.position=a.position;
        this.salary=a.salary;
    }
    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPosition() {
        return position;
    }

    public void setPosition(String position) {
        this.position = position;
    }

    public double getSalary() {
        return salary;
    }

    public void setSalary(double salary) {
        this.salary = salary;
    }
    public static class EmployeeDesignBuilder{
        private int id;
        private String name;
        private String position;
        private double salary;
        public EmployeeDesignBuilder(int id, String name) {
            this.id=id;
            this.name=name;
        }
        public EmployeeDesignBuilder withPosition(String position) {
            this.position=position;
            return this;
        }
        public EmployeeDesignBuilder withSalary(double salary) {
            this.salary=salary;
            return this;
        }
        public EmployeeDesign build() {
            return new EmployeeDesign(this);
        }
    }
}

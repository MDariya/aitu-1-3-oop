package kz.aitu.oop.examples.assignment7.subtask1;

import kz.aitu.oop.examples.assignment7.subtask1.Shape;

public class Circle extends Shape {
    protected double radius;
    public Circle() {
        super();
        radius=1.0;
    }
    public Circle(double radius) {
        this.radius = radius;
    }
    public Circle(String color, boolean filled, double radius) {
        super(color, filled);
        this.radius = radius;
    }


    public double getRadius() {
        return radius;
    }
    public void setRadius(double radius) {
        this.radius = radius;
    }

    @Override
    public double getArea() {
        return 3.14*radius*radius;
    }
    @Override
    public double getPerimeter() {
        return 2*3.14*radius;
    }
    @Override
    public String to_String() {
        return "Circle["+super.to_String()+"radius="+radius+"]";
    }

}

package kz.aitu.oop.examples.assignment7.subtask3;

public interface Resizable {
    public void resize (int percent);
}

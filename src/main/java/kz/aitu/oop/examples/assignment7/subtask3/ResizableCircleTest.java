package kz.aitu.oop.examples.assignment7.subtask3;

public class ResizableCircleTest {
    public static void main(String[] args) {
        ResizableCircle cr1 = new ResizableCircle(6.0);
        System.out.println(cr1.getArea());
        System.out.println(cr1.getPerimeter());
        System.out.println(cr1.to_String());
        cr1.resize(88);
        System.out.println("After resize");
        System.out.println(cr1.getArea());
        System.out.println(cr1.getPerimeter());
        System.out.println(cr1.to_String());
    }
}

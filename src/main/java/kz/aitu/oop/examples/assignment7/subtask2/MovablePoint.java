package kz.aitu.oop.examples.assignment7.subtask2;

public class MovablePoint implements Movable {
    int x;
    int y;
    int xSpeed;
    int ySpeed;

    public MovablePoint(int x, int y, int xSpeed, int ySpeed) {
        this.x = x;
        this.y = y;
        this.xSpeed = xSpeed;
        this.ySpeed = ySpeed;
    }
    public String to_String(){
        return "the point is on ("+x+", "+y+")";
    }
    public void moveUP() {y++;}
    public void moveDown(){y--;}
    public void moveLeft(){x--;}
    public void moveRight(){x++;}
}

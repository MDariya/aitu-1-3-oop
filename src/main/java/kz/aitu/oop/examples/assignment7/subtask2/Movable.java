package kz.aitu.oop.examples.assignment7.subtask2;

public interface Movable {
    public void moveUP();
    public void moveDown();
    public void moveLeft();
    public void moveRight();
}

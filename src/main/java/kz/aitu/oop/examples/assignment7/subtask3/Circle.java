package kz.aitu.oop.examples.assignment7.subtask3;

public class Circle implements GeometricObject{
    protected double radius=1.0;


    public Circle(double radius) {
        this.radius = radius;
    }
    public double getPerimeter() {
        return 2*3.14*radius;
    }
    public double getArea() {
        return 3.14*radius*radius;
    }
    public String to_String() {
        return "A circle with radius "+radius;
    }
}

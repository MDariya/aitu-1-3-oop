package kz.aitu.oop.examples.assignment7.subtask3;

public class ResizableCircle extends Circle implements Resizable {
    public ResizableCircle(double radius) {
        super(radius);
    }
    @Override
    public String to_String() {
        return "A resizable circle with radius "+radius;
    }
    public void resize(int percent) {
        radius=(radius*percent)/100;
    }
}

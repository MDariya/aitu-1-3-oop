package kz.aitu.oop.examples.assignment7.subtask2;

public class MovableCircle extends MovablePoint implements Movable {
    private int radius;
    private MovablePoint center;


    public MovableCircle(int x, int y, int xSpeed, int ySpeed, int radius, MovablePoint center) {
        super(x, y, xSpeed, ySpeed);
        this.radius = radius;
        this.center = center;
    }
    public String to_String() {
        return "Circle with radius "+radius+"and center ("+x+", "+y+")";
    }
    public void moveUP() {center.moveUP();}
    public void moveDown() {center.moveDown();}
    public void moveLeft() {center.moveLeft();}
    public void moveRight() {center.moveRight();}
}

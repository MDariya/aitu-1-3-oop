package kz.aitu.oop.examples.assignment7.subtask3;

public class Test {
    public static void main(String[] args) {
        Circle c1 = new Circle(5.0);
        System.out.println(c1.getArea());
        System.out.println(c1.getPerimeter());
        System.out.println(c1.to_String());

    }
}

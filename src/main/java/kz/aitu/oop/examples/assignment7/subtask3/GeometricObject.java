package kz.aitu.oop.examples.assignment7.subtask3;

public interface GeometricObject {
    public double getPerimeter();
    public double getArea();
}

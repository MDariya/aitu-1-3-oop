package kz.aitu.oop.examples.practice5Design;

public class SemiPreciousStoneDesign {
    private int id;
    private String type;
    private double weight;
    private double price;
    private SemiPreciousStoneDesign(SemiPreciousStoneDesignBuilder a) {
        this.id=a.id;
        this.type=a.type;
        this.price=a.price;
        this.weight=a.weight;
    }
    public int getId() {
        return id;
    }

    public String getType() {
        return type;
    }

    public double getWeight() {
        return weight;
    }

    public double getPrice() {
        return price;
    }
    private static class SemiPreciousStoneDesignBuilder {
        private int id;
        private String type;
        private double weight;
        private double price;
        public SemiPreciousStoneDesignBuilder (int id, String type) {
            this.id=id;
            this.type=type;
        }
        public SemiPreciousStoneDesignBuilder withWeight (double weight) {
            this.weight=weight;
            return this;
        }
        public SemiPreciousStoneDesignBuilder withPrice (double price) {
            this.price=price;
            return this;
        }
        public SemiPreciousStoneDesign build() {
            return new SemiPreciousStoneDesign (this);
        }
    }
}

package kz.aitu.oop.examples.practice5Design;

import kz.aitu.oop.examples.practice4Design.AquariumDesign;
import kz.aitu.oop.examples.practice5.Necklace;
import kz.aitu.oop.examples.practice5.PreciousStones;
import kz.aitu.oop.examples.practice5.SemiPreciousStones;

import java.util.LinkedList;

public class NecklaceDesign {
    LinkedList<PreciousStoneDesign> pStones=new LinkedList<>();
    LinkedList<SemiPreciousStoneDesign> spStones=new LinkedList<>();
    private static NecklaceDesign instance=null;
    private NecklaceDesign() {

    }
    public static NecklaceDesign getInstance() {
        if (instance==null) {
            instance = new NecklaceDesign();
        }
        return instance;
    }
    public LinkedList<PreciousStoneDesign> getpStones() {
        return pStones;
    }

    public LinkedList<SemiPreciousStoneDesign> getSpStones() {
        return spStones;
    }
    public void addPreciousStone(PreciousStoneDesign a) {
        pStones.push(a);
    }
    public void addSemiPreciousStone(SemiPreciousStoneDesign a) {
        spStones.push(a);
    }
    public double calculateTotalPrice() {
        double price=0.0;
        for (int i=0;i<spStones.size();i++) {
            price=price+spStones.get(i).getPrice();
        }
        for (int i=0;i<pStones.size();i++) {
            price=price+pStones.get(i).getPrice();
        }
        return price;
    }
    public double calculateTotalWeight() {
        double weight=0.0;
        for (int i=0;i<spStones.size();i++) {
            weight=weight+spStones.get(i).getWeight();
        }
        for (int i=0;i<pStones.size();i++) {
            weight=weight+pStones.get(i).getWeight();
        }
        return weight;
    }

}

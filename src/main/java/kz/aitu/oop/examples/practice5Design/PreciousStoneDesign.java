package kz.aitu.oop.examples.practice5Design;

import kz.aitu.oop.examples.practice2Design.RailwayCarriageDesign;

public class PreciousStoneDesign {
    private int id;
    private String type;
    private double weight;
    private double price;

    private PreciousStoneDesign(PreciousStoneDesignBuilder a) {
        this.id=a.id;
        this.type=a.type;
        this.price=a.price;
        this.weight=a.weight;
    }
    public int getId() {
        return id;
    }

    public String getType() {
        return type;
    }

    public double getWeight() {
        return weight;
    }

    public double getPrice() {
        return price;
    }
    public static class PreciousStoneDesignBuilder {
        private int id;
        private String type;
        private double weight;
        private double price;
        public PreciousStoneDesignBuilder (int id, String type) {
            this.id=id;
            this.type=type;
        }
        public PreciousStoneDesignBuilder withWeight (double weight) {
            this.weight=weight;
            return this;
        }
        public PreciousStoneDesignBuilder withPrice (double price) {
            this.price=price;
            return this;
        }
        public PreciousStoneDesign build() {
            return new PreciousStoneDesign (this);
        }
    }
}

package kz.aitu.oop.examples.polymorphism1914;

import lombok.Data;

@Data
public class Animal {

    private String name;

    public Animal(String name) {
        this.name = name;
    }

    public void eat() {
        System.out.println("Animal " + this.getName() + " eating");
    }
}

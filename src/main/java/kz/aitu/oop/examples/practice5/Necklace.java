package kz.aitu.oop.examples.practice5;

import java.util.LinkedList;

public class Necklace {
    LinkedList<PreciousStones> pStones=new LinkedList<>();
    LinkedList<SemiPreciousStones> spStones=new LinkedList<>();

    public Necklace() {
    }

    public LinkedList<PreciousStones> getpStones() {
        return pStones;
    }

    public LinkedList<SemiPreciousStones> getSpStones() {
        return spStones;
    }
    public void addPreciousStone(PreciousStones a) {
        pStones.push(a);
    }
    public void addSemiPreciousStone(SemiPreciousStones a) {
        spStones.push(a);
    }
    public double calculateTotalPrice() {
        double price=0.0;
        for (int i=0;i<spStones.size();i++) {
            price=price+spStones.get(i).getPrice();
        }
        for (int i=0;i<pStones.size();i++) {
            price=price+pStones.get(i).getPrice();
        }
        return price;
    }
    public double calculateTotalWeight() {
        double weight=0.0;
        for (int i=0;i<spStones.size();i++) {
            weight=weight+spStones.get(i).getWeight();
        }
        for (int i=0;i<pStones.size();i++) {
            weight=weight+pStones.get(i).getWeight();
        }
        return weight;
    }

}

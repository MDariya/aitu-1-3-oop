package kz.aitu.oop.examples.practice5;

import java.sql.*;
public class Main {
    public static void main(String[] args) {
        String url="jdbc:mysql://localhost:3307/practice2";
        String username="root";
        String pass="280102";
        try{
            Connection connection = DriverManager.getConnection(url, username, pass);
            Statement statement=connection.createStatement();
            ResultSet prstones=statement.executeQuery("select * from prestones");
            Necklace necklace=new Necklace();
            while (prstones.next()) {
                PreciousStones a = new PreciousStones(prstones.getInt(1), prstones.getString(2), prstones.getDouble(3), prstones.getDouble(4));
                necklace.addPreciousStone(a);
            }
            ResultSet spStones=statement.executeQuery("select * from spstones");
            while (spStones.next()) {
                SemiPreciousStones a=new SemiPreciousStones(spStones.getInt(1),spStones.getString(2),spStones.getDouble(3),spStones.getDouble(4));
                necklace.addSemiPreciousStone(a);
            }
            System.out.println(necklace.calculateTotalPrice());
            System.out.println(necklace.calculateTotalWeight());
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }
    }
}

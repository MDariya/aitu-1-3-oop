package kz.aitu.oop.examples.practice2;

public class Train {
    private CarriageCompound CarCompound;
    private Locomotive Loc;

    public Train(CarriageCompound carCompound, Locomotive loc) {
        CarCompound = carCompound;
        Loc = loc;
    }

    public CarriageCompound getCarCompound() {
        return CarCompound;
    }
    public Locomotive getLoc() {
        return Loc;
    }

    public double displayCapacity() {
        double capacity=0.0;
        for (int i=0;i<CarCompound.getNumber();i++) {
            capacity=capacity+CarCompound.getCarriage(i).getCapacity();
        }
        return capacity;
    }
}

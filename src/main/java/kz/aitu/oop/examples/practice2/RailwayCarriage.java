package kz.aitu.oop.examples.practice2;

public class RailwayCarriage {
private int id;
private String type;
private boolean available;
private double capacity;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public boolean isAvailable() {
        return available;
    }

    public void setAvailable(boolean available) {
        this.available = available;
    }

    public double getCapacity() {
        return capacity;
    }

    public void setCapacity(double capacity) {
        this.capacity = capacity;
    }

    public RailwayCarriage(int id, String type, boolean available, double capacity) {
        this.id = id;
        this.type = type;
        this.available = available;
        this.capacity = capacity;
    }
    public RailwayCarriage() {}

    public void infoAbout() {
        System.out.println("id:"+id);
        System.out.println("type: "+type);
        System.out.println("available: "+available);
        System.out.println("capacity: "+capacity);
        System.out.println("-----------");
    }

}

package kz.aitu.oop.examples.practice2;

import java.sql.*;
public class Main {
    public static void main(String[] args) {
        String url="jdbc:mysql://localhost:3307/practice2";
        String username="root";
        String pass="280102";
        try {
            Connection conn=DriverManager.getConnection(url, username,pass);
            Statement statement=conn.createStatement();
            ResultSet res=statement.executeQuery("select * from railcar");
            CarriageCompound passCR=new CarriageCompound();
            CarriageCompound carCR=new CarriageCompound();
            while (res.next()){
                RailwayCarriage r=new RailwayCarriage(res.getInt("id"),res.getString("type"),
                        res.getBoolean("available"),res.getDouble("capacity"));
                if (r.getType().equalsIgnoreCase("passanger")) {
                    passCR.addCarriage(r);
                } else if (r.getType().equalsIgnoreCase("cargo")) {
                    carCR.addCarriage(r);
                }
            }
            System.out.println(passCR.getCapacity());
            System.out.println(carCR.getCapacity());
            ResultSet resultSet=statement.executeQuery("select * from locomotive");
            while (resultSet.next()) {
                Locomotive L=new Locomotive(resultSet.getInt("id"),resultSet.getString("type"),
                        resultSet.getBoolean("available"));
                if (L.getType().equalsIgnoreCase("passanger")) {
                    Train PR=new Train(passCR, L);
                    System.out.println(PR.displayCapacity());
                } else {
                    Train CR=new Train (carCR,L);
                    System.out.println(CR.displayCapacity());
                }
            }
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }

    }
}

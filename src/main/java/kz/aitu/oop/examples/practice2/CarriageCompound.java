package kz.aitu.oop.examples.practice2;

import java.util.LinkedList;

public class CarriageCompound {
    LinkedList<RailwayCarriage> compound=new LinkedList<>();

    public CarriageCompound() {}

    public void addCarriage(RailwayCarriage a) {
        compound.push(a);
    }
    public RailwayCarriage getCarriage(int index) {
        return compound.get(index);
    }
    public int getNumber() {
        return compound.size();
    }
    public double getCapacity() {
        double capacity=0.0;
        for (int i=0;i<compound.size();i++) {
            capacity=capacity+compound.get(i).getCapacity();
        }
        return capacity;
    }
}

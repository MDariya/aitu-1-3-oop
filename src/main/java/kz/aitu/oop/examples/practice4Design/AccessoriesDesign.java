package kz.aitu.oop.examples.practice4Design;

public class AccessoriesDesign {
    private int id;
    private String type;
    private double cost;
    public AccessoriesDesign (AccessoriesDesignBuilder a) {
        this.id=id;
        this.type=type;
        this.cost=cost;
    }

    public int getId() {
        return id;
    }

    public String getType() {
        return type;
    }

    public double getCost() {
        return cost;
    }

    public class AccessoriesDesignBuilder {
        private int id;
        private String type;
        private double cost;

        public  AccessoriesDesignBuilder (int id) {
            this.id=id;
        }
        public  AccessoriesDesignBuilder withType(String type) {
            this.type=type;
            return this;
        }
        public  AccessoriesDesignBuilder withCost(double cost) {
            this.cost=cost;
            return this;
        }
        public AccessoriesDesign build() {
            return new  AccessoriesDesign(this);
        }

    }
}

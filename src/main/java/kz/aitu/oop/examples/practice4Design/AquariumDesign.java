package kz.aitu.oop.examples.practice4Design;

import kz.aitu.oop.examples.practice4.Accessories;
import kz.aitu.oop.examples.practice4.Fish;
import kz.aitu.oop.examples.practice4.Reptelies;

import java.util.LinkedList;

public class AquariumDesign {
    LinkedList<FishDesign> fish=new LinkedList<>();
    LinkedList<RepteliesDesign> reptelies=new LinkedList<>();
    LinkedList<AccessoriesDesign> accessories=new LinkedList<>();
    private static AquariumDesign instance=null;

    private AquariumDesign () {

    }
    public static AquariumDesign getInstance() {
        if (instance==null) {
            instance = new AquariumDesign();
        }
        return instance;
    }
    public LinkedList<FishDesign> getFish() {
        return fish;
    }

    public LinkedList<RepteliesDesign> getReptelies() {
        return reptelies;
    }

    public LinkedList<AccessoriesDesign> getAccessories() {
        return accessories;
    }
    public double totalCost() {
        double cost=0.0;
        for (int i=0;i<fish.size();i++) {
            cost=cost+fish.get(i).getCost();
        }
        for (int i=0;i<reptelies.size();i++) {
            cost=cost+reptelies.get(i).getCost();
        }
        for (int i=0;i<accessories.size();i++) {
            cost=cost+accessories.get(i).getCost();
        }
        return cost;
    }
    public void addFish(FishDesign a) {
        fish.push(a);
    }
    public void addAccessories(AccessoriesDesign a) {
        accessories.push(a);
    }
    public void addReptelies(RepteliesDesign a) {
        reptelies.push(a);
    }

}

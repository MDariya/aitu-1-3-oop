package kz.aitu.oop.examples.practice4Design;

public class FishDesign {
    private int id;
    private String type;
    private double cost;
    public FishDesign (FishDesignBuilder a) {

    }

    public int getId() {
        return id;
    }

    public String getType() {
        return type;
    }

    public double getCost() {
        return cost;
    }

    public static class FishDesignBuilder{
        private int id;
        private String type;
        private double cost;

        public FishDesignBuilder (int id) {
            this.id=id;
        }
        public FishDesignBuilder withType(String type) {
            this.type=type;
            return this;
        }
        public FishDesignBuilder withCost(double cost) {
            this.cost=cost;
            return this;
        }
        public FishDesign build() {
            return new FishDesign(this);
        }

    }
}

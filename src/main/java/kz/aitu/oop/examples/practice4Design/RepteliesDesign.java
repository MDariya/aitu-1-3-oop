package kz.aitu.oop.examples.practice4Design;

public class RepteliesDesign {
    private int id;
    private String type;
    private double cost;
    public RepteliesDesign (RepteliesDesignBuilder a) {
        this.id=id;
        this.type=type;
        this.cost=cost;
    }
    public int getId() {
        return id;
    }



    public String getType() {
        return type;
    }



    public double getCost() {
        return cost;
    }


    public class RepteliesDesignBuilder {
        private int id;
        private String type;
        private double cost;

        public RepteliesDesignBuilder (int id) {
            this.id=id;
        }
        public RepteliesDesignBuilder withType(String type) {
            this.type=type;
            return this;
        }
        public RepteliesDesignBuilder withCost(double cost) {
            this.cost=cost;
            return this;
        }
        public RepteliesDesign build() {
            return new RepteliesDesign(this);
        }

    }
}

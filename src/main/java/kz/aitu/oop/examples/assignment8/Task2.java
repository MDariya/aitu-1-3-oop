package kz.aitu.oop.examples.assignment8;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;

public class Task2 {
    public boolean isInt(String a) {
        try {
            int num=Integer.parseInt(a);
            return true;
        } catch (NumberFormatException e) {
            System.out.println("Number format exception");
            return false;
        }
    }
    public boolean isDouble (String a) {
        try {
            double e=Double.parseDouble(a);
            return true;
        } catch (NumberFormatException e) {
            System.out.println("Number format exception");
            return false;
        }
    }
    public boolean fileExists (String a) {
        try {
            File b=new File (a);
            FileReader r=new FileReader(b);
            return true;
        } catch (FileNotFoundException e) {
            System.out.println("File not found");
            return false;
        }
    }
}

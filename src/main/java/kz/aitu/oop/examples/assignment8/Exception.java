package kz.aitu.oop.examples.assignment8;

public class Exception {
    private String str;
    public Exception(String str) {
        this.str = str;
    }

    public String getStr() {
        return str;
    }

    public void setStr(String str) {
        this.str = str;
    }
}

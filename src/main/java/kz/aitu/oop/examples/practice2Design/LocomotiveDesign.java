package kz.aitu.oop.examples.practice2Design;

public class LocomotiveDesign {
    private int id;
    private String type;
    private boolean available;

    public LocomotiveDesign (LocomotiveDesignBuilder a) {
        this.id=a.id;
        this.type=a.type;
        this.available=a.available;
    }
    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public boolean isAvailable() {
        return available;
    }

    public void setAvailable(boolean available) {
        this.available = available;
    }
    public static class LocomotiveDesignBuilder {
        private int id;
        private String type;
        private boolean available;

        public LocomotiveDesignBuilder (int id, String type) {
            this.id=id;
            this.type=type;
        }

        public LocomotiveDesignBuilder withAvailable(boolean available) {
            this.available=available;
            return this;
        }

        public LocomotiveDesign build() {
            return new LocomotiveDesign(this);
        }
    }

}

package kz.aitu.oop.examples.practice2Design;

import kz.aitu.oop.examples.practice2.RailwayCarriage;

public class RailwayCarriageDesign {
    private int id;
    private String type;
    private boolean available;
    private double capacity;

    private RailwayCarriageDesign(RailwayCarriageDesignBuilder a) {
        this.id=a.id;
        this.type=a.type;
        this.available=a.available;
        this.capacity=a.capacity;
    }

    public int getId() {
        return id;
    }


    public String getType() {
        return type;
    }



    public boolean isAvailable() {
        return available;
    }


    public double getCapacity() {
        return capacity;
    }

    public static class RailwayCarriageDesignBuilder {
        private int id;
        private String type;
        private boolean available;
        private double capacity;

        public RailwayCarriageDesignBuilder (int id, String type) {
            this.id=id;
            this.type=type;
        }
        public RailwayCarriageDesignBuilder withCapacity (double capacity) {
            this.capacity=capacity;
            return this;
        }
        public RailwayCarriageDesignBuilder withAvailable (boolean available) {
            this.available=available;
            return this;
        }
        public RailwayCarriageDesign build() {
            return new RailwayCarriageDesign(this);
        }
    }
}

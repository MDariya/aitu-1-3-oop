package kz.aitu.oop.examples.practice2Design;

import kz.aitu.oop.examples.practice2.CarriageCompound;
import kz.aitu.oop.examples.practice2.Locomotive;

public class TrainDesign {
    private CarriageCompoundDesign CarCompound;
    private LocomotiveDesign Loc;

    public TrainDesign(CarriageCompoundDesign carCompound, LocomotiveDesign loc) {
        this.CarCompound = carCompound;
        this.Loc = loc;
    }

    public CarriageCompoundDesign getCarCompound() {
        return CarCompound;
    }
    public LocomotiveDesign getLoc() {
        return Loc;
    }

    public double displayCapacity() {
        double capacity=0.0;
        for (int i=0;i<CarCompound.getNumber();i++) {
            capacity=capacity+CarCompound.getCarriage(i).getCapacity();
        }
        return capacity;
    }
}

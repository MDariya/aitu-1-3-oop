package kz.aitu.oop.examples.practice2Design;

import kz.aitu.oop.examples.practice2.RailwayCarriage;

import java.util.LinkedList;

public class CarriageCompoundDesign {
    LinkedList<RailwayCarriageDesign> compound=new LinkedList<>();

    public CarriageCompoundDesign() {}

    public void addCarriage(RailwayCarriageDesign a) {
        compound.push(a);
    }
    public RailwayCarriageDesign getCarriage(int index) {
        return compound.get(index);
    }
    public int getNumber() {
        return compound.size();
    }
    public double getCapacity() {
        double capacity=0.0;
        for (int i=0;i<compound.size();i++) {
            capacity=capacity+compound.get(i).getCapacity();
        }
        return capacity;
    }
}

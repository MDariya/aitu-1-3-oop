package kz.aitu.oop.examples.practice6QUIZ;

import com.sun.xml.internal.bind.v2.runtime.unmarshaller.XsiNilLoader;

public class Singleton {
    private static Singleton instance=null;
    public String str;
    private Singleton() {
    }

    public static Singleton getSingleInstance() {
        if (instance == null) {
            instance = new Singleton();
        }
        return instance;
    }

    public String to_String() {
        return "Hello I am singleton! Let me say "+str+" to you ";
    }
}
